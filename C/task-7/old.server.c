#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/ipc.h>
#include <string.h>
#include <signal.h>

#define NMAX 256

int str_ptr = 0;
int str_ctr = 0;
int semid, shmid,exid;
pid_t pid = 0;

void str_fun(char *str);
void handler(int sig);

int main(int argc, char **argv)
{
	key_t key;
	char *shmaddr;
	char str[NMAX];
	int *isExit;
	struct sembuf sem;

	key = ftok("/etc/passwd", 'A');
	if ((semid = semget(key, 4, 0666|IPC_CREAT)) < 0) { printf("ERROR\n"); exit(1); }
	printf("dwd\n");
	if ((shmid = shmget(key, NMAX, 0666|IPC_CREAT)) < 0) { printf("ERROR\n"); exit(1); }
/*	if ((exid = shmget(key, sizeof(int), 0666|IPC_CREAT|IPC_EXCL)) < 0) { printf("ERROR\n"); exit(1); }*/
	shmaddr = shmat(shmid, NULL, 0);
	semctl(semid, 0, SETVAL, (int)1);
	semctl(semid, 1, SETVAL, (int)0);
	semctl(semid, 2, SETVAL, (int)1);
	semctl(semid, 3, SETVAL, (int)0);
	if ((pid = fork()) == 0)
	{
		sem.sem_op = -1;
		sem.sem_num = 3;
		semop(semid, &sem , 1);
		kill(getppid(), SIGUSR1);
		exit(0);
	}
	while (1)
	{
		printf("circle\n");
/*		sem.sem_op = -1; *//* Проверка на наличие в памяти строки */
/*		sem.sem_num = 1;
		semop(semid, &sem, 1);*/
		sem.sem_op = -1; /* Проверка на занятость ресурса */
		sem.sem_num = 0;
		semop(semid, &sem, 1);
		strcpy(str, shmaddr);
		sem.sem_op = 1; /* Убираем флаг занятости памяти для сервера */
		sem.sem_num = 1;
		semop(semid, &sem, 1);
		sem.sem_op = 1; /* Убираем флаг занятости памяти ддля клиентов */
		sem.sem_num = 2;
		semop(semid, &sem, 2);
		sem.sem_op = 1; /* Открываем доступ к памяти */
		sem.sem_num = 0;
		semop(semid, &sem, 1);
		str_fun(str);
	}
	return 0;
}

void str_fun(char *str)
{
	int i;
	char *ser = argv[1];

	for (i = 0; i < strlen(str); i++)
	{
		if (str_ptr >= strlen("SERVER"))
		{
			str_ctr++;
			str_ptr = 0;
		}
		if (str[i] == ser[str_ptr])
		{
			str_ptr++;
		}
		else
		{
			str_ptr = 0;
			if (str[i] == ser[str_ptr])
				str_ptr++;
		}
	}
	return;
}

void handler(int sig)
{
	if (sig == SIGUSR1)
	{
		semctl(semid, 0, IPC_RMID, (int)0);
		shmctl(shmid, IPC_RMID, 0);
		waitpid(pid, NULL, 0);
		exit(0);
	}
}
