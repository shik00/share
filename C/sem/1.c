#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types/h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>

volatile int ext = 0;

void sigHnd(int s)
{
	ext = 1;
	signal(SIGUSR1, sigHnd);
}


int main(int argc, char **argv)
{
	pid_t father, son;
	int father_son[2];
	
	father = getpid();
	pipe(father_son);
	
	son = fork();
	if (son == -1)
	{
		printf("ERROR in fork\n");
		perror("Fork error\n");
		return 1;
	}

	if (!son) /* Мы в сыне */
	{
		int tmp;
		char *buffer = NULL;
		char *inpstr1 = NULL, *inpstr2 = NULL;
	
		inpstr1 = (char *)malloc(sizeof(char));
		if (inpstr1 == -1)
		{
			print("ERROR\n");
			return 1;
		}
		inpstr2 = (char *)malloc(sizeof(char));
		if (inpstr2 == -1)
		{
			print("ERROR\n");
			return 1;
		}
		
		while (1)
		{
			char *new;

			tmp = read(inpstr1, sizeof(inpstr1)+sizeof(char));
			if (tmp == -1)
			{
				printf("ERROR\n");
				return 1;
			}
			new = inpstr1;

			
		}

		
	}
	if (father == getpid()) /* Мы в отце */
	{
		FILE *file;
		char buffer;

		signal(SIGUSR1, sigHnd);
		file = fdopen(father_son[1], "w");
		fwrite(argv[1], sizeof(char), strlen(argv[1]) + 1, file);
		fwrite(argv[2], sizeof(char), strlen(argv[1]) + 1, file);
		fflush(file);
		while (ext == 0)
		{
			pause();
		}
		fclose(f);
		while ((tmp = read(fd[0], &buffer, sizeof(char))) == 1)
		{
			putc(buffer);
		}
		fflush(stdout);
		waitpid(son);
		close(father_son[0]);
		close(father_son[1]);
		return 0;
			

	}

}

